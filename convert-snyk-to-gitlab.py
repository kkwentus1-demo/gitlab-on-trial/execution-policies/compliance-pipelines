import json
from types import SimpleNamespace

with open("snyk_data_file.json") as snyk_data_file:
    snyk_data = json.load(snyk_data_file, object_hook=lambda d: SimpleNamespace(**d))

gitlab_vulns = []
dependency_files = []
for i in snyk_data:
    dependency_files.append({"path": i.path, "package_manager": i.packageManager, "dependencies": []})
    for v in i.vulnerabilities:
        gitlab_identifiers = []
        for vuln_type, vuln_names in v.identifiers.__dict__.items():
            if vuln_names: 
                for vuln_name in vuln_names:
                    gitlab_identifiers.append({"type": vuln_type, "name": vuln_name, "value": vuln_name.partition("-")[2]})
                gitlab_vulns.append({"id": v.id, "category": "dependency_scanning", "severity": v.severity.capitalize(), "identifiers": gitlab_identifiers, "description": v.description, "location": {"file": i.displayTargetFile, "dependency": {"package": {"name": "PLACEHOLDER"}, "version": "PLACEHOLDER"}}})

# Dummy data for scan and dependency files 
full_json = {"version": "15.0.6", "dependency_files": dependency_files, "scan": {"analyzer": {"id": "snyk", "name": "Snyk", "vendor": {"name": "Snyk"}, "version": "1.0.2"}, "scanner": {"id": "my-snyk-scanner", "name": "My Snyk Scanner", "version": "1.0.2", "vendor": {"name": "Snyk"}}, "end_time": "2022-01-28T03:26:02", "start_time": "2020-01-28T03:26:02", "status": "success", "type": "dependency_scanning"}, "vulnerabilities": gitlab_vulns}

with open("gl-dependency-scanning-report.json", "w") as gitlab_file:
    json.dump(full_json, gitlab_file, default=vars)